<%@include file="/init.jsp" %>

<%
	User user2 = (User) request.getAttribute(WebKeys.SAMPLE_USER);
	user2.toString();
%>


<%-- <aui:field-wrapper label="label-name"> per quando si ha a che fare con dei CAMPI --%>

<liferay-ui:header title="<%= user2.getFullName() %>" backURL="<%= redirect %>" />

<%-- Mio
<liferay-portlet:renderURL var="backURL">
	<liferay-portlet:param name="mvcPath" value="/view.jsp" />
</liferay-portlet:renderURL>
 --%>

<aui:field-wrapper label="Informazioni utente">
	<aui:column columnWidth="50" first="true">
		<liferay-ui:message key="first-name" />: <%= user2.getFirstName() %><br />
		<liferay-ui:message key="last-name" />: <%= user2.getLastName() %><br />
		<liferay-ui:message key="email-address" />: <%= user2.getEmailAddress() %><br />
	</aui:column>
</aui:field-wrapper>

<%-- Mio
<a href="<%= backURL.toString() %>" >
	Indietro
</a>
 --%>