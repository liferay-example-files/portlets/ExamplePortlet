package it.unisef.sample.hook.listeners;

import com.liferay.portal.ModelListenerException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.model.BaseModelListener;
import com.liferay.portal.model.User;

public class UserListener extends BaseModelListener<User> {

	@Override
	public void onAfterCreate(User model) throws ModelListenerException {

		super.onAfterCreate(model);

		_log.info("Creato utente: " + model);

	}

	private static Log _log = LogFactoryUtil.getLog(UserListener.class);

}
