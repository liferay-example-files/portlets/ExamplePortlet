package it.unisef.sample.hook.events;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.liferay.portal.kernel.events.Action;
import com.liferay.portal.kernel.events.ActionException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.model.Portlet;

public class LoginPostAction extends Action {

	@Override
	public void run(HttpServletRequest arg0, HttpServletResponse arg1)
			throws ActionException {

		_log.info("run() di LoginPostAction");
	}

	private static Log _log = LogFactoryUtil.getLog(LoginPostAction.class);
}
